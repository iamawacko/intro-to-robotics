#!/usr/bin/env pybricks-micropython
from pybricks.hubs import EV3Brick
from pybricks.ev3devices import (Motor, TouchSensor, ColorSensor,
                                 InfraredSensor, UltrasonicSensor, GyroSensor)
from pybricks.parameters import Port, Stop, Direction, Button, Color
from pybricks.tools import wait, StopWatch, DataLog
from pybricks.robotics import DriveBase
from pybricks.media.ev3dev import SoundFile, ImageFile


# This program requires LEGO EV3 MicroPython v2.0 or higher.
# Click "Open user guide" on the EV3 extension tab for more information.


# Create your objects here.
ev3 = EV3Brick()


# Write your program here
while not Button.RIGHT in ev3.buttons.pressed():
    if (Button.UP in ev3.buttons.pressed()):
     ev3.speaker.say("GNU/Linux is good")
     ev3.speaker.say("Even having systemd")
     ev3.speaker.say("Free software is great")
    if (Button.DOWN in ev3.buttons.pressed()):
        ev3.speakers.soundfile(HORN_2)