#!/usr/bin/env pybricks-micropython
from pybricks.hubs import EV3Brick
from pybricks.ev3devices import (Motor, TouchSensor, ColorSensor,
                                 InfraredSensor, UltrasonicSensor, GyroSensor)
from pybricks.parameters import Port, Stop, Direction, Button, Color
from pybricks.tools import wait, StopWatch, DataLog
from pybricks.robotics import DriveBase
from pybricks.media.ev3dev import SoundFile, ImageFile, Font


# This program requires LEGO EV3 MicroPython v2.0 or higher.
# Click "Open user guide" on the EV3 extension tab for more information.


# Create your objects here.
ev3 = EV3Brick()


# Write your program here.
ev3.screen.set_font(Font(None, 8))
ev3.screen.print("You are hungry, but there isn't much food ")
ev3.screen.print("Left to eat pepperoni pizza")
ev3.screen.print("Right to eat pineapple pizza")
while not Button.CENTER in ev3.buttons.pressed():
    if (Button.RIGHT in ev3.buttons.pressed()):
        ev3.screen.clear()
        ev3.screen.print("You eat the pineapple pizza")
        ev3.screen.print("It didn't taste very good")
        ev3.screen.print("But at least you aren't dead")
        wait(5000)
        ev3.screen.clear()
        ev3.screen.print("You are getting a bit thirsty ")
        ev3.screen.print("Right to drink water")
        ev3.screen.print("Left to drink milk")
        while not Button.CENTER in ev3.buttons.pressed():
            if (Button.RIGHT in ev3.buttons.pressed()):
                ev3.screen.clear()
                ev3.screen.print("Your thirst is sated")
                wait(5000)
            if (Button.LEFT in ev3.buttons.pressed()):
                ev3.screen.clear()
                ev3.screen.print("The spirit of the cow bursts forth") 
                ev3.screen.print("and eradicates you")
                ev3.speaker.say("Dead")
                wait(5000)
    if (Button.LEFT in ev3.buttons.pressed()):
        ev3.screen.clear()
        ev3.screen.print("You made the wrong choice!")
        ev3.screen.print("As you greedily consume the food")
        ev3.screen.print("the spirits of the animals burst forth")
        ev3.screen.print("they render you deceased")
        ev3.speaker.say("Dead")
        wait(500)
        ev3.screen.clear()
        ev3.screen.print("Your in hell now. What do you do?")
        ev3.screen.print("Right to leave")
        ev3.screen.print("Left to stay")
        while not Button.CENTER in ev3.buttons.pressed():
            if (Button.LEFT in ev3.buttons.pressed()):
                ev3.screen.clear()
                ev3.screen.print("You made the wrong choice") 
                ev3.screen.print("You are tortured forever")
                wait(5000)
            if (Button.RIGHT in ev3.buttons.pressed()):
                ev3.screen.clear()
                ev3.screen.print("It wasn't exactly the right choice,") 
                ev3.screen.print("but it wasn't the wrong one ")
                ev3.screen.print(" you escape and die for real")
                wait(5000)

