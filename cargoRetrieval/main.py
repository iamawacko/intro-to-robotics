#!/usr/bin/env pybricks-micropython
#You can find the rest of my code at https://gitlab.com/iamawacko/intro-to-robotics
from pybricks.hubs import EV3Brick
from pybricks.ev3devices import (Motor, TouchSensor, ColorSensor,
                                 InfraredSensor, UltrasonicSensor, GyroSensor)
from pybricks.parameters import Port, Stop, Direction, Button, Color
from pybricks.tools import wait, StopWatch, DataLog
from pybricks.robotics import DriveBase
from pybricks.media.ev3dev import SoundFile, ImageFile


# This program requires LEGO EV3 MicroPython v2.0 or higher.
# Click "Open user guide" on the EV3 extension tab for more information.


# Create your objects here.
ev3 = EV3Brick()


# Write your program here.


left_motor = Motor(Port.B)
right_motor = Motor(Port.C)

robot = DriveBase(left_motor, right_motor, wheel_diameter=55.5, axle_track=152)
robot.settings(1000, 1000)
robot_arm = Motor(Port.D, Direction.COUNTERCLOCKWISE, [12, 20])

#Drive to the ball
robot.straight(500)

#Pick up the ball
robot_arm.run_angle(900, 400)

#Turn around and drive back to start.
robot.turn(155)
robot.straight(518)

#Drop off the ball
robot_arm.run_angle(900, -600)
