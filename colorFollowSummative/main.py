#!/usr/bin/env pybricks-micropython
from pybricks.hubs import EV3Brick
from pybricks.ev3devices import (Motor, TouchSensor, ColorSensor,
                                 InfraredSensor, UltrasonicSensor, GyroSensor)
from pybricks.parameters import Port, Stop, Direction, Button, Color
from pybricks.tools import wait, StopWatch, DataLog
from pybricks.robotics import DriveBase
from pybricks.media.ev3dev import SoundFile, ImageFile


# This program requires LEGO EV3 MicroPython v2.0 or higher.
# Click "Open user guide" on the EV3 extension tab for more information.


# Create your objects here.
ev3 = EV3Brick()

left_motor = Motor(Port.B)
right_motor = Motor(Port.C)
color = ColorSensor(Port.S2)

#ev3.robot.settings()

blue = Color.BLUE
red = Color.RED
black = 4
white = 17
threshold = (black + white) / 2

robot = DriveBase(left_motor, right_motor, wheel_diameter=55.5, axle_track=160)

while Button.CENTER not in ev3.buttons.pressed():
    diff = color.reflection() - threshold
    turn = diff * 2.45
    robot.drive(30, turn)
    colors = color.color()
    if colors == blue:
        ev3.speaker.say("Time to turn around")
        robot.turn(180)
    elif colors == red:
        break
    wait(1)
