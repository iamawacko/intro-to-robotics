#!/usr/bin/env pybricks-micropython
from pybricks.hubs import EV3Brick
from pybricks.ev3devices import (Motor, TouchSensor, ColorSensor,
                                 InfraredSensor, UltrasonicSensor, GyroSensor)
from pybricks.parameters import Port, Stop, Direction, Button, Color
from pybricks.tools import wait, StopWatch, DataLog
from pybricks.robotics import DriveBase
from pybricks.media.ev3dev import SoundFile, ImageFile


# This program requires LEGO EV3 MicroPython v2.0 or higher.
# Click "Open user guide" on the EV3 extension tab for more information.


# Create your objects here.
ev3 = EV3Brick()

left_motor = Motor(Port.B)
right_motor = Motor(Port.C)
color = ColorSensor(Port.S2)

#ev3.robot.settings()

robot = DriveBase(left_motor, right_motor, wheel_diameter=55.5, axle_track=160)

while Button.CENTER not in ev3.buttons.pressed():
    if color.reflection() < 15:
        robot.drive(500, 40)
    elif color.reflection() < 75:
        robot.drive(500, 0)
    elif color.reflection() > 50:
        robot.drive(500, -40)
    wait(1)
