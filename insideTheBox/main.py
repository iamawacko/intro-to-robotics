#!/usr/bin/env pybricks-micropython
from pybricks.hubs import EV3Brick
from pybricks.ev3devices import (Motor, TouchSensor, ColorSensor,
                                 InfraredSensor, UltrasonicSensor, GyroSensor)
from pybricks.parameters import Port, Stop, Direction, Button, Color
from pybricks.tools import wait, StopWatch, DataLog
from pybricks.robotics import DriveBase
from pybricks.media.ev3dev import SoundFile, ImageFile


# This program requires LEGO EV3 MicroPython v2.0 or higher.
# Click "Open user guide" on the EV3 extension tab for more information.


# Create your objects here.
ev3 = EV3Brick()

sensor = UltrasonicSensor(Port.S4)
left_motor = Motor(Port.B)
right_motor = Motor(Port.C)
touch_sensor = TouchSensor(Port.S3)

#ev3.robot.settings()

robot = DriveBase(left_motor, right_motor, wheel_diameter=55.5, axle_track=160)

turn = 82
speed = 1000
wall = 1
distance = 0

while True:
    robot.drive(speed, 0)
    wait(1)
    distance += 1
    if sensor.distance() < 300:
        speed = 180
    if sensor.distance() > 300:
        speed = 1000
    if touch_sensor.pressed():
        robot.straight(-50)
        robot.turn(turn)
        ev3.screen.print("wall")
        ev3.screen.print(wall)
        wait(1000)
        ev3.screen.clear()
        if wall == 1:
            a = distance - 50
        if wall == 2:
            b = distance - a - 50
        wall += 1
    #This bit of code should make the robot return home 
    if wall == 5:
        speed = 1000
        slow = 400
        robot.turn(turn)
        robot.drive(speed, 0)
        wait(a)
        robot.turn(turn)
        robot.drive(speed, 0)
        wait(b)
        ev3.screen.print("I should be home now")
        break


