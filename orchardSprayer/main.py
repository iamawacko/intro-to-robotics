#!/usr/bin/env pybricks-micropython
#You can find the rest of my code at https://gitlab.com/iamawacko/intro-to-robotics
from pybricks.hubs import EV3Brick
from pybricks.ev3devices import (Motor, TouchSensor, ColorSensor,
                                 InfraredSensor, UltrasonicSensor, GyroSensor)
from pybricks.parameters import Port, Stop, Direction, Button, Color
from pybricks.tools import wait, StopWatch, DataLog
from pybricks.robotics import DriveBase
from pybricks.media.ev3dev import SoundFile, ImageFile


# This program requires LEGO EV3 MicroPython v2.0 or higher.
# Click "Open user guide" on the EV3 extension tab for more information.


# Create your objects here.
ev3 = EV3Brick()


# Write your program here.


left_motor = Motor(Port.B)
right_motor = Motor(Port.C)

robot = DriveBase(left_motor, right_motor, wheel_diameter=55.5, axle_track=152)
robot.settings(1000, 1000)
robot_arm = Motor(Port.D, Direction.CLOCKWISE, [12, 20])

#First line of trees, the 56 CM ones
robot_arm.run_angle(90, 90)
robot.straight(750)
robot.turn(-82)
robot.straight(200)
robot.turn(-82)
robot.straight(730)
robot_arm.run_angle(90, -90)

#This is the code to move to the next line of trees
robot.turn(90)
robot_arm.run_angle(90, 90)
robot.turn(52)

#This is the code to do the 2nd line of trees, the 88 CM one
robot.drive(600, -3)
wait(3000)
robot.turn(-82)
robot.straight(400)
robot.turn(-82)
robot.straight(900)
robot_arm.run_angle(90, -90)

#This is the code to move to the next line of trees
robot.turn(100)
robot.straight(100)
robot.turn(35)

#This is the code to do the 3rd line of trees, the 69 CM one.
robot_arm.run_angle(90, 90)
robot.straight(850)
robot.turn(-82)
robot.straight(300)
robot.turn(-82)
robot.straight(850)
robot_arm.run_angle(90, -90)
