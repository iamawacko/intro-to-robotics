#!/usr/bin/env pybricks-micropython
from pybricks.hubs import EV3Brick
from pybricks.ev3devices import (Motor, TouchSensor, ColorSensor,
                                 InfraredSensor, UltrasonicSensor, GyroSensor)
from pybricks.parameters import Port, Stop, Direction, Button, Color
from pybricks.tools import wait, StopWatch, DataLog
from pybricks.robotics import DriveBase
from pybricks.media.ev3dev import SoundFile, ImageFile


# This program requires LEGO EV3 MicroPython v2.0 or higher.
# Click "Open user guide" on the EV3 extension tab for more information.


# Create your objects here.
ev3 = EV3Brick()

left_motor = Motor(Port.B)
right_motor = Motor(Port.C)

#ev3.robot.settings()

robot = DriveBase(left_motor, right_motor, wheel_diameter=55.5, axle_track=160)

k = 1
s = 0
# I saw my  partner using lists, so I decided to use a dictionary for the important stuff
#I still need a list to get the best score though
random = ["This is a list", "This was a list", "This will be a list"]
#This dictionary is bascially acting like a list.
dictionary = {
        0: "Linux is good",
        1: "Vim is better \n than EMACS",
        2: "Vim is better \n than VScode",
        3: "Microservices \n are better than monoliths",
}
#Tuple's are cool
tuple1 = ("this", "is", "a", "tuple")
#For loop
for x in range(4):
    #Wait before the screen is cleared, so people can read the screen
    wait(50)
    ev3.screen.clear()
    #Movement code
    robot.straight(820)
    robot.turn(89)
    #Lambda's are great. Any chance we could use haskell in class?
    f = lambda a : a / 9.5
    #Calculate pi to more precise values as we get closer to the end
    for i in range(1000*x):
        if i % 2 == 0:
            s += 4/k
        else:
            s -= 4/k
        k += 2

    #Say and print the words in the dictionary
    ev3.speaker.say(dictionary[x])
    ev3.screen.print(dictionary[x])
    #Print pi
    ev3.screen.print(s)
    #Print the number of entries in my tuple
    ev3.screen.print(len(tuple1))
    #Print my tuple
    ev3.screen.print(tuple1)
    #Use the lambda
    ev3.speaker.say("{0}".format(f(s)))
    #Say something from the list. I need to use a list to get a good grade
    ev3.speaker.say(random[x])
