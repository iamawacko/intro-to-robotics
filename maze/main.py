#!/usr/bin/env pybricks-micropython
#You can find the rest of my code at https://gitlab.com/iamawacko/intro-to-robotics
from pybricks.hubs import EV3Brick
from pybricks.ev3devices import (Motor, TouchSensor, ColorSensor,
                                 InfraredSensor, UltrasonicSensor, GyroSensor)
from pybricks.parameters import Port, Stop, Direction, Button, Color
from pybricks.tools import wait, StopWatch, DataLog
from pybricks.robotics import DriveBase
from pybricks.media.ev3dev import SoundFile, ImageFile


# This program requires LEGO EV3 MicroPython v2.0 or higher.
# Click "Open user guide" on the EV3 extension tab for more information.


# Create your objects here.
ev3 = EV3Brick()


# Write your program here.


left_motor = Motor(Port.B)
right_motor = Motor(Port.C)

robot = DriveBase(left_motor, right_motor, wheel_diameter=55.5, axle_track=152)

robot.settings(1000, 1000)

robot.straight(600)
robot.turn(89)
robot.straight(400)
robot.turn(89)
robot.straight(400)
robot.turn(-89)
robot.straight(800)
robot.turn(-89)
robot.straight(750)
robot.turn(-89)
robot.straight(1100)
robot.turn(89)
robot.straight(10000)

